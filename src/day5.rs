use std::io::BufReader;
use std::fs::File;
use std::io::prelude::*;
use std::io::Lines;
use regex::Regex;

fn main() {
    let file = File::open("input/day5.txt").unwrap();
    let mut lines = BufReader::new(file).lines();

    let mut stacks: Vec<Vec<String>> = Vec::new();

    build_stacks(&mut lines, &mut stacks);
    move_items(&mut lines, &mut stacks);

    stacks.into_iter().map(|stack| stack[stack.len()-1].clone()).map(|top| print!("{top}")).count();
}

fn build_stacks(lines: &mut Lines<BufReader<File>>, stacks: &mut Vec<Vec<String>>) {
    for line in lines {
        let layer = line.unwrap();
        if !layer.contains("[") {
            break;
        }

        for (i, stack_item) in layer.chars().enumerate() {
            if ('A'..='Z').contains(&stack_item) {
                let stack_index = (i + 1) / 4;
                if stacks.len() < stack_index + 1 {
                    for _ in stacks.len()..stack_index + 1 {
                        stacks.push(Vec::new());
                    }
                }
                stacks[stack_index].insert(0, stack_item.to_string());
            }
        }
    }
}


fn move_items(lines: &mut Lines<BufReader<File>>, stacks: &mut Vec<Vec<String>>) {
    for line in lines {
        let directions = line.unwrap();
        if !directions.starts_with("move") {
            continue;
        }
        let re = Regex::new(r"(\d*)").unwrap();
        let args: Vec<usize> = re.captures_iter(&directions)
            .filter_map(|cap| cap[1].parse::<usize>().ok())
            .collect();

        if args.len() == 3 {
            move_item_in_bulk(stacks, args[0], args[1]-1, args[2]-1);
        }
    }
}

fn move_item_in_bulk(stacks: &mut Vec<Vec<String>>, how_many: usize, from_index: usize, to_index: usize) {
    let from_stack = &mut stacks[from_index];
    let tail = from_stack.split_off(from_stack.len() - how_many);
    stacks[to_index].extend(tail);
}