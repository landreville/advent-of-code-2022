use std::cmp;
use std::fs::File;
use std::io::BufReader;
use std::io::BufRead;
use regex::Regex;
use rayon::prelude::*;

fn main() {
    let blueprints = load_blueprints();

    let sum = blueprints.par_iter().enumerate().map(|(i, blueprint)| {
        let maxes = max_necessary(blueprint);
        let ctx = Context {
            inv: [0; 4],
            bots: [1, 0, 0, 0],
            minutes_left: 24,
            maxes
        };
        let geode_count = run_blueprint(blueprint, ctx, 0, None);

        geode_count * blueprint.index
    }).sum::<usize>();

    println!("Sum: {sum}");
}

fn run_blueprint(
    blueprint: &BluePrint,
    ctx: Context,
    prev_best: usize,
    prev_skipped: Option<&Vec<usize>>
) -> usize {
    // Last minute, count the geodes we will make and return it as a shortcut
    if ctx.minutes_left == 1 {
        return ctx.inv[3] + ctx.bots[3];
    }

    // Prune if it's impossible to beat prev_best
    if best_case(&ctx) < prev_best {
        return 0;
    }

    // Next state
    let mut next_ctx = ctx.clone();
    // Count down
    next_ctx.minutes_left -= 1;
    // Add material inventory
    for (mat, bot_count) in next_ctx.bots.iter().enumerate() {
        next_ctx.inv[mat] += bot_count;
    }

    // If we can make a geode bot, then make it
    if can_make(&next_ctx, &blueprint.bot_prints[Material::Geode as usize]) {
        for (i, cost) in blueprint.bot_prints[Material::Geode as usize].costs.iter().enumerate() {
            next_ctx.inv[i] -= cost;
        }
        next_ctx.bots[Material::Geode as usize] += 1;

        return run_blueprint(blueprint, next_ctx, prev_best, None);
    }

    let mut best = prev_best;
    // Which bots can we make
    let bot_opts: Vec<usize> = (0..3).filter(|i|
        can_make(&ctx, &blueprint.bot_prints[*i])
    ).collect();

    // Try each
    for mat in &bot_opts {
        // Didn't make this last iteration, so don't bother this time either
        if prev_skipped.map(|ps| ps.contains(&mat)).unwrap_or(false) {
            continue;
        }

        // Make a new state for the next run
        let mut next_ctx = next_ctx.clone();
        // Subtract inventory for new bot
        for (i, cost) in blueprint.bot_prints[*mat].costs.iter().enumerate() {
            next_ctx.inv[i] -= cost;
        }
        // Add new bot
        next_ctx.bots[*mat] += 1;

        // Follow branch
        let score = run_blueprint(blueprint, next_ctx, best, None);
        // Was it worth it?
        best = cmp::max(best, score);
    }

    // Follow branch where we don't create any new bot
    // (this is always the first iteration when we have no inventory yet)
    let score = run_blueprint(blueprint, next_ctx, best, Some(&bot_opts));
    best = cmp::max(best, score);

    best
}

fn can_make(ctx: &Context, bot_print: &BotPrint) -> bool {
    let bot_mat = bot_print.bot_type as usize;
    let no_more = ctx.bots[bot_mat] >= ctx.maxes[bot_mat];
    !no_more && bot_print.costs.iter().enumerate().all(|(mat, cost)| ctx.inv[mat] >= *cost)
}

fn best_case(ctx: &Context) -> usize {
    let mat = 3;
    let i = ctx.minutes_left;
    ctx.inv[mat] + ctx.bots[mat] * i + i * (i-1) / 2
}

fn max_necessary(blueprint: &BluePrint) -> [usize; 4] {
    let mut maxes = [0, 0, 0, usize::MAX];

    for mat in 0..4 {
        for bp in 0..4 {
            maxes[mat] = cmp::max(maxes[mat], blueprint.bot_prints[bp].costs[mat]);
        }
    }
    maxes
}

#[derive(Debug, Copy, Clone)]
struct Context {
    inv: [usize; 4],
    bots: [usize; 4],
    minutes_left: usize,
    maxes: [usize; 4]
}

#[derive(Copy, Clone)]
enum Material {
    Ore = 0,
    Clay = 1,
    Obsidian = 2,
    Geode = 3,
}

struct BluePrint {
    index: usize,
    bot_prints: [BotPrint; 4],
}

struct BotPrint {
    costs: [usize; 4],
    bot_type: Material
}


fn load_blueprints() -> Vec<BluePrint> {
    let file = File::open("input/day19.txt").unwrap();
    let lines = BufReader::new(file).lines();
    let re = Regex::new(r"Blueprint (?P<blueprint>\d+): Each ore robot costs (?P<orebotorecost>\d+) ore. Each clay robot costs (?P<claybotorecost>\d+) ore. Each obsidian robot costs (?P<obsbotorecost>\d+) ore and (?P<obsbotclaycost>\d+) clay. Each geode robot costs (?P<geobotorecost>\d+) ore and (?P<geobotobscost>\d+) obsidian.").unwrap();
    let mut bps = Vec::new();
    for (i, line) in lines.enumerate() {
        let line = line.unwrap();
        let caps = re.captures(line.as_str()).unwrap();
        bps.push(BluePrint {
            index: i+1,
            bot_prints: [
                BotPrint {
                    bot_type: Material::Ore,
                    costs: [caps.name("orebotorecost").unwrap().as_str().parse::<usize>().unwrap(), 0, 0, 0],
                },
                BotPrint {
                    bot_type: Material::Clay,
                    costs: [caps.name("claybotorecost").unwrap().as_str().parse::<usize>().unwrap(), 0, 0, 0],
                },
                BotPrint {
                    bot_type: Material::Obsidian,
                    costs: [
                        caps.name("obsbotorecost").unwrap().as_str().parse::<usize>().unwrap(),
                        caps.name("obsbotclaycost").unwrap().as_str().parse::<usize>().unwrap(),
                        0,
                        0
                    ],
                },
                BotPrint {
                    bot_type: Material::Geode,
                    costs: [
                        caps.name("geobotorecost").unwrap().as_str().parse::<usize>().unwrap(),
                        0,
                        caps.name("geobotobscost").unwrap().as_str().parse::<usize>().unwrap(),
                        0
                    ],
                },
            ]
        });
    }
    bps
}
