use std::fs::File;
use std::io::SeekFrom;
use std::io::Read;
use std::io::Seek;

fn main() {
    let mut file = File::open("input/day6.txt").unwrap();
    let mut buffer = [0; 14];
    let mut after_marker = 0;

    loop {
        file.read(&mut buffer[..]).unwrap();
        if let Ok(marker) = std::str::from_utf8(&buffer) {
            if has_unique_chars(&marker) {
                break;
            }
        }
        after_marker = file.seek(
            SeekFrom::Current(-1 * (buffer.len() as i64 - 1))
        ).unwrap() + buffer.len() as u64;
    }
    println!("Marker position: {after_marker}");
}

fn has_unique_chars(marker: &str) -> bool {
    let mut chars: Vec<char> = marker.chars().collect();
    chars.sort();
    let full_len = chars.len();
    chars.dedup();
    full_len == chars.len()
}

