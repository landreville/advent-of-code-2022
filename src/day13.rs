use std::cmp::Ordering;
use std::fs::File;
use std::io::BufReader;
use std::io::BufRead;
use serde_json::{Value};
use serde_json;

fn main() {
    let mut packets = load_packets();
    let mut packets_iter = packets.iter();
    let mut i = 0;
    let mut correct = Vec::new();

    while let Some(left_value) = packets_iter.next() {
        i += 1;
        let right_value = packets_iter.next().unwrap();
        let result = compare_packets(&left_value, &right_value);
        if result != Ordering::Greater {
            correct.push(i);
        }
    }

    let sum: i32 = correct.iter().sum();
    println!("Sum correct: {sum}");

    let dividers = vec![serde_json::from_str("[[2]]").unwrap(), serde_json::from_str("[[6]]").unwrap()];
    packets.extend(dividers.clone());
    packets.sort_by(|l, r| compare_packets(&l, &r));

    let decoder_a_i = packets.iter().position(|el| *el == dividers[0]).unwrap();
    let decoder_b_i = packets.iter().position(|el| *el == dividers[1]).unwrap();
    let decoder = (decoder_b_i+1) * (decoder_a_i+1);

    println!("Decoder: {decoder}");
}

fn compare_packets(l: &Value, r: &Value) -> Ordering {
    let mut result: Ordering = Ordering::Equal;
    if l.is_array() && r.is_array() {
        let l_list = l.as_array().unwrap();
        let r_list = r.as_array().unwrap();

        let mut found = false;
        for (l, r) in Iterator::zip(l_list.iter(), r_list.iter()) {
            let el_cmp = compare_packets(l, r);
            if el_cmp != Ordering::Equal {
                found = true;
                result = el_cmp;
                break;
            }
        }
        if !found {
            result = l_list.len().cmp(&r_list.len());
        }
    } else if l.is_array() || r.is_array() {
        let l = ensure_iter(l);
        let r = ensure_iter(r);
        result = compare_packets(&l, &r);
    } else if l.is_number() && r.is_number() {
        result = l.as_i64().unwrap().cmp(&r.as_i64().unwrap());
    } else {
        panic!("Unknown types.");
    }
    result
}

fn ensure_iter(a: &Value) -> Value {
    if a.is_array() {
        a.clone()
    } else {
        Value::Array(vec![a.clone()])
    }
}

fn load_packets() -> Vec<Value> {
    let file = File::open("input/day13.txt").unwrap();
    let lines = BufReader::new(file).lines();
    let mut packets = Vec::new();

    for line in lines {
        let line = line.unwrap();
        if line.len() == 0 {
            continue;
        }
        let packet = serde_json::from_str(line.as_str()).unwrap();

        packets.push(packet);
    }
    packets
}