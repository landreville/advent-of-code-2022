use std::cmp;
use std::collections::HashSet;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use regex::Regex;

fn main() {
    let cubes = load_cubes();
    let exposed = flood_fill(&cubes);
    let open = cubes.iter()
        .flat_map(|c| neighbours(*c))
        .filter(|c| exposed.contains(c))
        .count();

    println!("Exposed faces: {open:?}");
}

fn flood_fill(cubes: &Vec<(i32, i32, i32)>) -> HashSet<(i32, i32, i32)> {
    let bounds = bounds(&cubes);
    let mut exposed = HashSet::new();
    let start = (0, 0, 0);
    let mut stack = Vec::new();
    let mut seen = HashSet::new();

    stack.push(start);
    seen.insert(start);

    while let Some(point) = stack.pop() {
        for neighbour in neighbours(point) {
            if cubes.contains(&neighbour) || !hit_boundary(&bounds, &neighbour) {
                continue;
            }
            if seen.insert(neighbour) {
                stack.push(neighbour);
                exposed.insert(neighbour);
            }
        }
    }

    exposed
}

fn hit_boundary(bounds: &[(i32, i32, i32); 2], point: &(i32, i32, i32)) -> bool {
    let [min, max] = bounds;
    point.0 >= min.0 - 1
        && point.0 <= max.0 + 1
        && point.1 >= min.1 - 1
        && point.1 <= max.1 + 1
        && point.2 >= min.2 - 1
        && point.2 <= max.2 + 1
}

fn bounds(cubes: &Vec<(i32, i32, i32)>) -> [(i32, i32, i32); 2]{
    cubes.iter().fold(
        [(i32::MAX, i32::MAX, i32::MAX), (i32::MIN, i32::MIN, i32::MIN)],
        |[mut min, mut max], cube| {
            min.0 = cmp::min(min.0, cube.0);
            min.1 = cmp::min(min.1, cube.1);
            min.2 = cmp::min(min.2, cube.2);
            max.0 = cmp::max(max.0, cube.0);
            max.1 = cmp::max(max.1, cube.1);
            max.2 = cmp::max(max.2, cube.2);
            [min, max]
        }
    )
}

fn neighbours(cube: (i32, i32, i32)) -> Vec<(i32, i32, i32)> {
    let mut neighbours = Vec::new();
    for dimension in ['X', 'Y', 'Z'] {
        for offset in [-1, 1] {
            let mut neighbour = cube.clone();
            match dimension {
                'X' => neighbour.0 += offset,
                'Y' => neighbour.1 += offset,
                'Z' => neighbour.2 += offset,
                _ => panic!("WTF")
            }
            neighbours.push(neighbour);
        }
    }
    neighbours
}

fn load_cubes() -> Vec<(i32, i32, i32)> {
    let file = File::open("input/day18.txt").unwrap();
    let lines = BufReader::new(file).lines();
    let re = Regex::new(r"([0-9]+),([0-9]+),([0-9]+)").unwrap();
    let mut cubes = Vec::new();
    for line in lines {
        let line = line.unwrap();
        let cap = re.captures(line.as_str()).unwrap();
        cubes.push((
            cap[1].parse::<i32>().unwrap(),
            cap[2].parse::<i32>().unwrap(),
            cap[3].parse::<i32>().unwrap(),
        ));
    }
    cubes
}