use std::collections::VecDeque;
use std::io::BufReader;
use std::fs::File;
use std::io::prelude::*;
use std::io::Lines;


fn main() {
    let mut monkeys = load_monkeys();

    let lcm: u64 = monkeys.iter().map(|m| m.divisor).product();

    for _ in 0..10000 {
        for i in 0..monkeys.len() {
            while let Some(item) = monkeys[i].items.pop_front() {
                monkeys[i].inspection_count += 1;
                let worry_level = (monkeys[i].operation)(item) % lcm;

                let throw_to= if worry_level % monkeys[i].divisor == 0 {
                    *&monkeys[i].if_true_throw_to
                } else {
                    *&monkeys[i].if_false_throw_to
                };

                monkeys[throw_to].items.push_back(worry_level);
            }
        }
    }

    monkeys.sort_by(|a, b| b.inspection_count.cmp(&a.inspection_count));
    let monkey_business = &monkeys[0].inspection_count * &monkeys[1].inspection_count;
    println!("Monkey business: {monkey_business}");
}

struct Monkey {
    items: VecDeque<u64>,
    operation: Box<dyn Fn(u64) -> u64>,
    divisor: u64,
    if_true_throw_to: usize,
    if_false_throw_to: usize,
    inspection_count: u64,
}

fn load_monkeys() -> Vec<Monkey>{
    let file = File::open("input/day11.txt").unwrap();
    let mut lines= BufReader::new(file).lines();
    let mut monkeys:Vec<Monkey> = Vec::new();
    if let Some(Ok(_line)) = lines.next() {
        parse_monkey(&mut monkeys, &mut lines);
    }
    monkeys
}

fn parse_monkey(monkeys: &mut Vec<Monkey>, lines: &mut Lines<BufReader<File>>) {
    monkeys.push(Monkey {
        items: lines.next().unwrap().unwrap()[18..].split(", ")
            .map(|part| part.parse::<u64>().unwrap())
            .collect(),
        operation: parse_operation(&lines.next().unwrap().unwrap()[19..]),
        divisor: *&lines.next().unwrap().unwrap()[21..].parse::<u64>().unwrap(),
        if_true_throw_to: *&lines.next().unwrap().unwrap()[29..].parse::<usize>().unwrap(),
        if_false_throw_to: *&lines.next().unwrap().unwrap()[30..].parse::<usize>().unwrap(),
        inspection_count: 0,
    });

    while let Some(Ok(line)) = lines.next() {
        if line.starts_with("Monkey") {
            parse_monkey(monkeys, lines);
            break;
        }
    }
}

fn parse_operation(line: &str) -> Box<dyn Fn(u64) -> u64> {
    let line = line.to_string();
    let mut op = line[4..5].to_string();
    let mut value = 0;
    if line[6..].to_string() == "old" {
        op = "**".to_string();
    } else {
        value = line[6..].parse::<u64>().unwrap();
    }

    Box::new(move |old| {
        println!("{old:?}");
        match op.clone().as_str() {
            "*" => old * value,
            "+" => old + value,
            "**" => old * old,
            _ => panic!("Invalid operation")
        }
    })
}
