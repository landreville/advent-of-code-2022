use std::cmp::Reverse;
use std::collections::{BinaryHeap, HashMap, HashSet};
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

type ValleyState = [[[bool; 5]; 122]; 27];

fn main() {
    let initial_state = load_blizzard();
    let (cycle, cycle_start) = load_blizzard_cycle(&initial_state);
    let cycle_len = cycle.len() - cycle_start;

    let go = navigate_blizzard(&cycle, cycle_start, cycle_len, 0, (120, 26), (1, 0));
    let back = navigate_blizzard(&cycle, cycle_start, cycle_len, go, (1, 0), (120, 26));
    let total = navigate_blizzard(&cycle, cycle_start, cycle_len, back, (120, 26), (1, 0));
    println!("Total: {total}");
}

fn navigate_blizzard(
    cycle: &Vec<ValleyState>,
    cycle_start: usize,
    cycle_len: usize,
    minute: usize,
    end: (usize, usize),
    pos: (usize, usize),
) -> usize {
    let neighbours = [(-1, 0), (0, -1), (0, 0), (0, 1), (1, 0)];

    let mut seen: HashSet<(usize, usize, usize)> = HashSet::default();
    let mut to_visit = BinaryHeap::new();
    to_visit.push((
        Reverse(manhattan_distance(pos, end)),
        Reverse(minute),
        (pos.1, pos.0),
    ));

    while let Some((Reverse(_cost), Reverse(time), (y, x))) = to_visit.pop() {
        if (x, y) == end {
            return time;
        }

        let cycle_i = if time < cycle_start {
            time + 1
        } else {
            cycle_start + (time + 1 - cycle_start) % cycle_len
        };

        let state = cycle[cycle_i];
        for (rel_x, rel_y) in neighbours.iter().copied() {
            let Some(neigh_x) = x.checked_add_signed(rel_x) else {
                continue;
            };

            let Some(neigh_y) = y.checked_add_signed(rel_y) else {
                continue;
            };

            if neigh_y >= state.len() || neigh_x > state[neigh_y].len() {
                continue;
            }

            if state[neigh_y][neigh_x].iter().all(|&v| !v)
                && seen.insert((cycle_i, neigh_y, neigh_x))
            {
                let new_cost = manhattan_distance((neigh_x, neigh_y), end) + time + 1;
                to_visit.push((Reverse(new_cost), Reverse(time + 1), (neigh_y, neigh_x)));
            }
        }
    }

    0
}

fn manhattan_distance(a: (usize, usize), b: (usize, usize)) -> usize {
    a.1.abs_diff(b.1) + a.0.abs_diff(b.0)
}

fn next_blizzard(blizzard: &ValleyState) -> ValleyState {
    let mut next_blizzard = [[[false; 5]; 122]; 27];
    let right = 120;
    let left = 1;
    let top = 1;
    let bottom = 25;

    for (y, row) in blizzard.iter().enumerate() {
        for (x, _) in row.iter().enumerate() {
            let cur_space = blizzard[y][x];

            if cur_space[Valley::Wall as usize] {
                next_blizzard[y][x][Valley::Wall as usize] = true;
                continue;
            }

            if cur_space[Valley::Down as usize] {
                let y = if y == bottom { top } else { y + 1 };
                next_blizzard[y][x][Valley::Down as usize] = true;
            }
            if cur_space[Valley::Up as usize] {
                let y = if y == top { bottom } else { y - 1 };
                next_blizzard[y][x][Valley::Up as usize] = true;
            }
            if cur_space[Valley::Left as usize] {
                let x = if x == left { right } else { x - 1 };
                next_blizzard[y][x][Valley::Left as usize] = true;
            }
            if cur_space[Valley::Right as usize] {
                let x = if x == right { left } else { x + 1 };
                next_blizzard[y][x][Valley::Right as usize] = true;
            }
        }
    }

    next_blizzard
}

fn load_blizzard_cycle(initial_state: &ValleyState) -> (Vec<ValleyState>, usize) {
    let mut cycle_map = HashMap::new();
    let mut state = initial_state.clone();

    let cycle_start = loop {
        let next = next_blizzard(&state);
        let next_order = cycle_map.len();
        let cycle_num = *cycle_map.entry(state).or_insert(next_order);
        if cycle_num != next_order {
            break cycle_num;
        }
        state = next;
    };

    let mut cycle = vec![[[[false; 5]; 122]; 27]; cycle_map.len()];
    for (state, i) in cycle_map {
        cycle[i] = state;
    }
    (cycle, cycle_start)
}

#[derive(Debug, Clone, Copy)]
enum Valley {
    Up = 0,
    Down = 1,
    Left = 2,
    Right = 3,
    Wall = 4,
}

fn load_blizzard() -> ValleyState {
    let file = File::open("input/day24.txt").unwrap();
    let lines = BufReader::new(file).lines();
    let mut initial_state = [[[false; 5]; 122]; 27];

    for (y, line) in lines.enumerate() {
        let line = line.unwrap();
        for (x, ch) in line.chars().enumerate() {
            initial_state[y][x][match ch {
                '>' => Valley::Right as usize,
                '<' => Valley::Left as usize,
                '^' => Valley::Up as usize,
                'v' => Valley::Down as usize,
                '.' => continue,
                '#' => Valley::Wall as usize,
                _ => panic!("Invalid valley space state."),
            }] = true;
        }
    }

    initial_state
}
