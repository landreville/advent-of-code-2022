use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use std::collections::HashSet;

fn main() {
    let file = File::open("input/day3.txt").unwrap();
    let lines = BufReader::new(file).lines();
    let mut group_lines = vec![String::new(), String::new(), String::new()];
    let mut first_star_total: i32 = 0;
    let mut second_star_total: i32 =0;

    for (i, line) in lines.enumerate() {
        let line_value = line.unwrap();
        let items_in_both  = check_rucksack(&line_value);
        items_in_both.into_iter().map(|el| first_star_total += item_value(el)).count();

        if i % 3 == 0 && i != 0 {
            second_star_total += item_value(group_badge_item(&group_lines));
        }
        group_lines[i%3] = String::from(line_value);
    }
    second_star_total += item_value(group_badge_item(&group_lines));

    println!("First star: {first_star_total}");
    println!("Second star: {second_star_total}");
}

fn check_rucksack(items: &str) -> Vec<char> {
    let (first_comp, second_comp) = items.split_at(items.len() / 2);
    let first_set: HashSet<char> = HashSet::from_iter(first_comp.chars());
    let second_set: HashSet<char> = HashSet::from_iter(second_comp.chars());
    let in_both: Vec<char> = first_set.intersection(&second_set).into_iter().copied().collect();
    in_both
}

const LOWERCASE_A_ORD: i32 = 'a' as i32;
const UPPERCASE_A_ORD: i32 = 'A' as i32;

fn item_value(item: char) -> i32{
    let item_ord = item.clone() as i32;
    if  item_ord > LOWERCASE_A_ORD {
        item_ord - LOWERCASE_A_ORD + 1
    } else {
        item_ord - UPPERCASE_A_ORD + 27
    }
}

fn group_badge_item(group_lines: &Vec<String>) -> char {
    let mut item = '_';
    let mut missed;
    for group_item in group_lines[0].chars() {
        missed = false;
        for group_line in group_lines {
            if !group_line.contains(group_item) {
                missed = true;
                break;
            }
        }
        if !missed {
            item = group_item;
            break;
        }
    }
    item
}