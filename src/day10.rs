use std::io::BufReader;
use std::fs::File;
use std::io::prelude::*;

fn main() {
    let mut state = 1;
    let mut check_total = 0;
    let mut cycle = 0;

    for command in read_commands().iter() {
        draw_crt(&state, &cycle);
        cycle += 1;
        check_signal(&state, &mut check_total, &cycle);

        if let Command::Addx(value) = command {
            draw_crt(&state, &cycle);
            cycle += 1;
            check_signal(&state, &mut check_total, &cycle);
            state += value;
        }
    }

    println!("\nCycles sum: {check_total}");
}

fn draw_crt(state: &i32, cycle: &i32) {
    let draw_pixel = *cycle % 40;
    if (*state - 1..=*state + 1).contains(&draw_pixel) {
        print!("█");
    } else {
        print!(".");
    }
    if draw_pixel == 39 {
        print!("\n");
    }
}

fn check_signal(state: &i32, check_total: &mut i32, cycle: &i32) {
    if *cycle == 20 || (*cycle + 20) % 40 == 0 {
        *check_total += *state * *cycle;
    }
}

fn read_commands() -> Vec<Command> {
    let file = File::open("input/day10.txt").unwrap();
    let lines = BufReader::new(file).lines();
    lines.map(|l| read_command(l.unwrap())).collect()
}

enum Command {
    Noop,
    Addx(i32),
}

fn read_command(line: String) -> Command {
    if let Some((_, value)) = line.split_once(" ") {
        Command::Addx(value.parse::<i32>().unwrap())
    } else {
        Command::Noop
    }
}