use std::collections::HashSet;
use std::io::BufReader;
use std::fs::File;
use std::io::prelude::*;

fn main() {
    let directions = read_directions();
    let mut t_trail: HashSet<(i32, i32)> = HashSet::new();
    t_trail.insert((0, 0));
    // Length 10 for star 2, length 2 for star 1
    let mut positions = vec![(0, 0); 10];

    directions.iter().map(|dir| move_rope(&dir, &mut positions, &mut t_trail)).count();

    let move_count = t_trail.len();
    println!("Moves: {move_count}");
}

fn move_rope(
    dir: &Direction,
    positions: &mut Vec<(i32, i32)>,
    t_trail: &mut HashSet<(i32, i32)>,
) {
    positions[0] = move_head(dir, &positions[0]);
    let mut prev_knot = positions[0];
    for i in 1..positions.len() {
        positions[i] = move_knot(&prev_knot, &positions[i]);
        prev_knot = positions[i];
    }
    t_trail.insert(prev_knot);
}

fn move_head(dir: &Direction, head: &(i32, i32)) -> (i32, i32) {
    match dir {
        Direction::Up => (head.0, head.1 + 1),
        Direction::Right => (head.0 + 1, head.1),
        Direction::Down => (head.0, head.1 - 1),
        Direction::Left => (head.0 - 1, head.1),
    }
}

fn move_knot(prev_knot: &(i32, i32), this_knot: &(i32, i32)) -> (i32, i32) {
    let x_diff = prev_knot.0 - this_knot.0;
    let y_diff = prev_knot.1 - this_knot.1;
    let mut new_pos = (this_knot.0, this_knot.1);

    if prev_knot.0 == new_pos.0 && y_diff.abs() > 1 {
        // Aligned vertically, move up/down
        new_pos.1 += if y_diff > 0 { 1 } else { -1 };
    } else if prev_knot.1 == new_pos.1 && x_diff.abs() > 1 {
        // Aligned horizontally, move left/right
        new_pos.0 += if x_diff > 0 { 1 } else { -1 };
    } else if y_diff.abs() > 1 || x_diff.abs() > 1 {
        // Move diagonal
        new_pos.0 += if x_diff > 0 { 1 } else { -1 };
        new_pos.1 += if y_diff > 0 { 1 } else { -1 };
    }

    new_pos
}

#[derive(Clone)]
enum Direction {
    Up,
    Down,
    Right,
    Left,
}

fn read_directions() -> Vec<Direction> {
    let file = File::open("input/day9.txt").unwrap();
    let lines = BufReader::new(file).lines();
    lines.map(|l| l.unwrap())
        .flat_map(|l| parse_direction_line(l))
        .filter_map(|el| el)
        .collect()
}

fn parse_direction_line(line: String) -> Vec<Option<Direction>> {
    let (direction, steps) = line.split_once(" ").unwrap();
    let steps: usize = steps.parse::<usize>().unwrap();
    match (direction, steps) {
        ("U", steps) => vec![Some(Direction::Up); steps],
        ("R", steps) => vec![Some(Direction::Right); steps],
        ("D", steps) => vec![Some(Direction::Down); steps],
        ("L", steps) => vec![Some(Direction::Left); steps],
        _ => vec![None]
    }
}

