use std::collections::HashSet;
use std::fs::File;
use std::io::BufReader;
use std::io::BufRead;
use regex::Regex;
use std::cmp;

fn main() {
    let mut points = load_rock();
    let mut how_many = 0;
    let sand_from = (500, 0);
    let mut i = 0;

    let mut floor = 0;
    for p in &points {
        if p.1 > floor {
            floor = p.1;
        }
    }
    floor = floor + 2;

    'falling_grains: loop {
        i = i + 1;

        let mut before_move = sand_from.clone();
        let mut after_move;

        'move_grain: loop {
            if !points.contains(&(before_move.0, before_move.1 + 1)) &&
                before_move.1 + 1 < floor
            {
                after_move = (before_move.0, before_move.1 + 1);
            } else if !points.contains(&(before_move.0 - 1, before_move.1 + 1)) &&
                before_move.1 + 1 < floor
            {
                after_move = (before_move.0 - 1, before_move.1 + 1);
            } else if !points.contains(&(before_move.0 + 1, before_move.1 + 1)) &&
                before_move.1 + 1 < floor
            {
                after_move = (before_move.0 + 1, before_move.1 + 1);
            } else {
                points.insert(before_move);
                how_many += 1;
                if before_move == sand_from {
                    break 'falling_grains;
                }
                break 'move_grain;
            }
            before_move = after_move;
        }
    }

    println!("Settled grains: {how_many}");
}


fn load_rock() -> HashSet<(i32, i32)> {
    let point_re: Regex = Regex::new(r"(\d+),(\d+)").unwrap();
    let file = File::open("input/day14.txt").unwrap();
    let lines = BufReader::new(file).lines();
    let mut points = HashSet::new();

    for line in lines {
        let line = line.unwrap();
        let mut captures = point_re.captures_iter(line.as_str());
        let first_cap = captures.next().unwrap();
        let mut prev = (
            *&first_cap[1].parse::<i32>().unwrap(),
            *&first_cap[2].parse::<i32>().unwrap()
        );

        while let Some(cap) = captures.next() {
            let end = (
                *&cap[1].parse::<i32>().unwrap(),
                *&cap[2].parse::<i32>().unwrap(),
            );

            if prev.0 == end.0 {
                for y in cmp::min(prev.1, end.1)..=cmp::max(prev.1, end.1) {
                    points.insert((prev.0, y));
                }
            } else if prev.1 == end.1 {
                for x in cmp::min(prev.0, end.0)..=cmp::max(prev.0, end.0) {
                    points.insert((x, prev.1));
                }
            }

            prev = end;
        }
    }
    points
}