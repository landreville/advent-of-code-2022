use std::fs::File;
use std::io::BufReader;
use std::io::BufRead;

fn main() {
    let key = 811589153;
    let vals: Vec<isize> = load_file().iter().map(|v| v*key).collect();

    let mut mangles: Vec<usize> = (0..vals.len()).collect();

    for _ in 0..10 {
        for (i, val) in vals.iter().enumerate() {
            let mangles_i = mangles.iter().position(|val_i| *val_i == i).unwrap();
            mangles.remove(mangles_i);
            let new_mangles_i = (mangles_i as isize + val).rem_euclid(mangles.len() as isize);
            mangles.insert(new_mangles_i as usize, i);
        }
    }

    let zero_i = vals.iter().position(|v| *v == 0).unwrap();
    let mangle_zero_i = mangles.iter().position(|val_i| *val_i == zero_i).unwrap();

    let total: isize = [1000, 2000, 3000].iter().map(|offset| {
        let mangle_i = (mangle_zero_i + offset) % mangles.len();
        let vals_i = mangles[mangle_i];
        vals[vals_i]
    }).sum();

    println!("Total: {total}");
}

fn load_file() -> Vec<isize> {
    let file = File::open("input/day20.txt").unwrap();
    let lines = BufReader::new(file).lines();
    let mut vals = Vec::new();
    for line in lines {
        let line = line.unwrap();
        vals.push(line.parse::<isize>().unwrap());
    }
    vals
}