use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

fn main() {
    let file = File::open("input/day2.txt").unwrap();
    let lines = BufReader::new(file).lines();
    let mut total_score = 0;
    for line in lines {
        if let Ok(strategy) = line {
            let mut vals = strategy.split_whitespace();
            let first = vals.next();
            let second = vals.next();
            if let (Some(their_choice), Some(desired_outcome)) = (first, second) {
                let outcome = desired_outcome.chars().next().unwrap() as i32  - 'X' as i32;
                let them =  their_choice.chars().next().unwrap() as i32  - 'A' as i32;
                let delta = match  (outcome, them) {
                    (0, 0) => 2,
                    (2, 2) => -2,
                    _ => outcome - 1
                };
                total_score += outcome * 3 + (them + delta + 1);
            }
        }
    }
    println!("Total score: {total_score}");
}