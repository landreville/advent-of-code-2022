use std::collections::{BinaryHeap, HashMap, HashSet};
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::cmp::Ordering;

use regex::Regex;

type AdjList<'a> = HashMap<&'a Valve, Vec<&'a Valve>>;
type DistanceMatrix<'a> = HashMap<String, HashMap<String, i32>>;

fn main() {
    let mut valves: Vec<Valve> = Vec::new();
    let adj = load_valves(&mut valves);
    let distances = distance_matrix(&adj);
    let start = *adj.keys().find(|k| &k.name == "AA").unwrap();
    let valves_to_open: Vec<&Valve> = adj.keys().filter(|v| v.flow > 0).map(|v| *v).collect::<Vec<&Valve>>();

    let PathFit { path: no_overlap, flow } = find_path(
        &adj,
        &valves_to_open,
        &distances,
        start,
        26,
        &vec![start],
        &Vec::new()
    );
    println!("Human Flow: {flow}");
    println!("{no_overlap:?}");

    let PathFit { path: _, flow: elephant_flow } = find_path(
        &adj,
        &valves_to_open,
        &distances,
        start,
        26,
        &vec![start],
        &no_overlap
    );
    println!("Elephant Flow: {elephant_flow}");
    let total = flow + elephant_flow;
    println!("Total: {total}");

}

fn find_path<'a>(
    valves: &'a AdjList,
    to_open: &Vec<&'a Valve>,
    distances: &'a DistanceMatrix,
    start: &'a Valve,
    minutes: i32,
    path: &Vec<&'a Valve>,
    no_overlap: &Vec<&'a Valve>
) -> PathFit<'a> {
    let mut paths: Vec<PathFit> = Vec::new();

    for valve in to_open {
        if no_overlap.contains(valve) {
            continue;
        }

        let distance = distances[&start.name][&valve.name];
        if distance >= minutes {
            continue;
        }
        let minutes_left = minutes - distance - 1;
        let flow = valve.flow * minutes_left;
        let next_to_open = &to_open.iter().filter(|v| *v != valve).map(|v| *v).collect();

        let mut next_path = path.clone();
        next_path.push(valve);
        let full_path = find_path(
            valves,
            next_to_open,
            distances,
            valve,
            minutes_left,
            &next_path,
            &no_overlap
        );
        let mut add_path = path.clone();
        add_path.extend(full_path.path);
        paths.push(PathFit { path: add_path, flow: full_path.flow + flow });
    }

    let mut best_path = PathFit { path: Vec::new(), flow: 0 };
    for path_fit in paths {
        if path_fit.flow > best_path.flow {
            best_path = path_fit;
        }
    }
    best_path
}

fn distance_matrix<'a>(valves: &'a AdjList) -> DistanceMatrix<'a> {
    let mut distances = HashMap::new();
    for start in valves.keys() {
        let start = *start;
        let distances_from = distances.entry(start.name.clone())
            .or_insert(HashMap::new());
        let mut to_visit = BinaryHeap::new();
        let mut visited = HashSet::new();

        to_visit.push(Visit { valve: start, distance: 0 });

        while let Some(Visit { valve, distance }) = to_visit.pop() {
            if !visited.insert(valve) {
                continue;
            }

            if let Some(neighbours) = valves.get(valve) {
                for neighbour in neighbours {
                    let neighbour = *neighbour;
                    let new_dist = distance + 1;
                    let use_dist = distances_from.get(&neighbour.name)
                        .map_or(true, |&current_dist| {
                            current_dist > new_dist
                        });
                    if use_dist {
                        distances_from.insert(neighbour.name.clone(), new_dist);
                        to_visit.push(Visit { valve: neighbour, distance: new_dist });
                    }
                }
            }
        }
    }
    distances
}

#[derive(Debug, Clone)]
struct PathFit<'a> {
    path: Vec<&'a Valve>,
    flow: i32,
}

#[derive(Debug, Eq, PartialEq, Hash, Clone)]
struct Valve {
    name: String,
    flow: i32,
}

#[derive(Debug)]
struct Visit<'a> {
    valve: &'a Valve,
    distance: i32,
}

impl<'a> Ord for Visit<'a> {
    fn cmp(&self, other: &Self) -> Ordering {
        other.distance.cmp(&self.distance)
    }
}

impl<'a> PartialOrd for Visit<'a> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl<'a> PartialEq for Visit<'a> {
    fn eq(&self, other: &Self) -> bool {
        self.distance.eq(&other.distance)
    }
}

impl<'a> Eq for Visit<'a> {}

fn load_valves(valves: &mut Vec<Valve>) -> HashMap<&Valve, Vec<&Valve>> {
    let file = File::open("input/day16.txt").unwrap();
    let lines = BufReader::new(file).lines();
    let re = Regex::new(r"Valve ([A-Z]+) has flow rate=([0-9]+); tunnels? leads? to valves? (.+)").unwrap();
    // let valves = Vec::new();
    let mut adj: HashMap<&Valve, Vec<&Valve>> = HashMap::new();
    let mut neighbours: HashMap<String, Vec<String>> = HashMap::new();

    for line in lines {
        let line = line.unwrap();
        let cap = re.captures(line.as_str()).unwrap();
        valves.push(Valve {
            name: cap[1].to_string(),
            flow: cap[2].parse::<i32>().unwrap(),
        });
        neighbours.insert(
            cap[1].to_string(),
            cap[3].split(", ").map(|s| s.to_string()).collect(),
        );
    }

    for valve in valves.iter() {
        let neighbour_list = neighbours.get(&valve.name).unwrap();
        for neighbour in neighbour_list {
            let nvalve = valves.iter().filter(|v| &v.name == neighbour).next().unwrap();
            adj.entry(valve).or_insert(Vec::new()).push(nvalve);
        }
    }
    adj
}