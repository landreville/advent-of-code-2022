use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use std::ops::Range;


fn main() {
    let file = File::open("input/day4.txt").unwrap();
    let lines = BufReader::new(file).lines();
    let mut overlap_pair_count = 0;
    let mut partial_pair_count = 0;

    for line in lines {
        let pair_line = line.unwrap();
        let (first_elf, second_elf) = pair_line.split_once(",").unwrap();
        let first_range = make_elf_range(&first_elf);
        let second_range = make_elf_range(&second_elf);
        let overlaps = range_full_overlaps(&first_range, &second_range);
        if overlaps {
            overlap_pair_count += 1;
        }
        let partial_overlap = partial_overlap(&first_range, &second_range);
        if partial_overlap {
            partial_pair_count += 1;
        }
    }

    println!("Overlapping pair count: {overlap_pair_count}");
    println!("Partial overlap count: {partial_pair_count}");
}

fn make_elf_range(elf_assignment: &str) -> Range<i32> {
    let (start, end) = elf_assignment.split_once("-").unwrap();
    start.parse::<i32>().unwrap()..end.parse::<i32>().unwrap()
}

fn range_full_overlaps(range1: &Range<i32>, range2: &Range<i32>) -> bool {
    (range1.start <= range2.start && range1.end >= range2.end) || (
        range2.start <= range1.start && range2.end >= range1.end
    )
}

fn partial_overlap(range1: &Range<i32>, range2: &Range<i32>) -> bool {
    range1.start <= range2.end && range1.end >= range2.start
}