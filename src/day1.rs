use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

fn main() {
    let mut top_three = vec![0, 0, 0];
    let mut current_elf_cals = 0;
    let file = File::open("input/day1.txt").unwrap();
    let lines = BufReader::new(file).lines();
    for line in lines {
        if let Ok(calories) = line {
            if let Ok(value) = calories.parse::<i32>() {
                current_elf_cals = current_elf_cals + value;
            } else {

                if current_elf_cals > top_three[0] {
                    top_three[0] = current_elf_cals;
                    top_three.sort_unstable();
                }
                current_elf_cals = 0;
            }
        }
    }

    println!("Top three: {top_three:#?}");
    let total: i32 = top_three.iter().sum();
    println!("Total: {total}");
}
