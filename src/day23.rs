use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

type Elves = [(isize, isize); 2801];

fn main() {
    let mut elves = load_elves();
    let mut pot_elves;
    let mut compass = [Compass::North, Compass::South, Compass::West, Compass::East]
        .iter()
        .cycle();

    let mut i = 0;
    let mut moved;
    loop {
        let current_compass = [
            compass.next().unwrap(),
            compass.next().unwrap(),
            compass.next().unwrap(),
            compass.next().unwrap(),
        ];
        pot_elves = elves.clone();
        make_potential_moves(&elves, &mut pot_elves, &current_compass);
        moved = resolve_elf_conflicts(&mut elves, &pot_elves);
        if !moved {
            println!("i: {i}");
            break;
        }
        compass.next();
        i += 1;
    }

    // count_tiles(&elves);
}

// fn count_tiles(elves: &Elves) {
//     let max_x = elves.iter().map(|(x, _)| x).max().unwrap() + 1;
//     let min_x = elves.iter().map(|(x, _)| x).min().unwrap();
//     let max_y = elves.iter().map(|(_, y)| y).max().unwrap() + 1;
//     let min_y = elves.iter().map(|(_, y)| y).min().unwrap();
//
//     let result = (max_x - min_x) * (max_y - min_y) - elves.len() as isize;
//     println!("Result: {result}. ({max_x}, {max_y}) ({min_x}, {min_y})");
// }

fn resolve_elf_conflicts(elves: &mut Elves, pot_elves: &Elves) -> bool {
    let mut moved = false;

    for (i, mv_elf) in pot_elves.iter().enumerate().filter_map(|(elf_i, (x, y))| {
        if pot_elves
            .iter()
            .enumerate()
            .find(|(oi, (ox, oy))| *oi != elf_i && *ox == *x && *oy == *y)
            == None
        {
            Some((elf_i, (*x, *y)))
        } else {
            None
        }
    }) {
        if elves[i] != mv_elf {
            moved = true;
        }
        elves[i] = mv_elf;
    }

    moved
}

fn make_potential_moves(elves: &Elves, pot_elves: &mut Elves, compass: &[&Compass; 4]) {
    // let mut potential_elves: Elves = elves.clone();

    for (i, mv_elf) in elves.iter().enumerate().filter_map(|(i, (x, y))| {
        let mut new_elf = None;
        let mut all_free = true;
        for dir in compass {
            if is_free(elves, *dir, *x, *y) {
                if new_elf == None {
                    new_elf = Some((i, move_elf(*dir, *x, *y)));
                }
            } else {
                all_free = false;
            }
        }

        if all_free {
            None
        } else {
            new_elf
        }
    }) {
        pot_elves[i] = mv_elf;
    }
}

fn is_free(elves: &Elves, compass: &Compass, x: isize, y: isize) -> bool {
    match compass {
        Compass::North => {
            elves
                .iter()
                .find(|(ex, ey)| *ey == y - 1 && x - 1 <= *ex && x + 1 >= *ex)
                == None
        }
        Compass::South => {
            elves
                .iter()
                .find(|(ex, ey)| *ey == y + 1 && x - 1 <= *ex && x + 1 >= *ex)
                == None
        }
        Compass::West => {
            elves
                .iter()
                .find(|(ex, ey)| *ex == x - 1 && y - 1 <= *ey && y + 1 >= *ey)
                == None
        }
        Compass::East => {
            elves
                .iter()
                .find(|(ex, ey)| *ex == x + 1 && y - 1 <= *ey && y + 1 >= *ey)
                == None
        }
    }
}

fn move_elf(compass: &Compass, x: isize, y: isize) -> (isize, isize) {
    let new_pos = match compass {
        Compass::North => (x, y - 1),
        Compass::South => (x, y + 1),
        Compass::West => (x - 1, y),
        Compass::East => (x + 1, y),
    };
    new_pos
}

#[derive(Debug, Clone, Copy)]
enum Compass {
    North = 0,
    South = 1,
    West = 2,
    East = 3,
}

fn load_elves() -> Elves {
    let file = File::open("input/day23.txt").unwrap();
    let lines = BufReader::new(file).lines();
    let mut elves: Elves = [(0, 0); 2801];
    let mut i = 0;

    for (y, line) in lines.enumerate() {
        let line = line.unwrap();
        for (x, ch) in line.chars().enumerate() {
            if ch == '#' {
                elves[i] = (x as isize, y as isize);
                i += 1;
            }
        }
    }
    elves
}
