use std::fs::File;
use std::io::BufReader;
use regex::Regex;
use std::io::BufRead;
use std::collections::{HashMap, HashSet};
use std::cmp::{min, max};
use std::ops::{Range, RangeInclusive};

fn main() {
    let sensors = load_sensors();
    let mut overlap_lines: Vec<Vec<RangeInclusive<i32>>> = Vec::new();
    let bound = 4000000;

    for scanner_y in 0..bound {
        let mut ranges = Vec::new();

        for sensor in &sensors {
            let mut y_overlap = 0;
            let (x, y) = sensor.location;
            let radius = sensor.radius();
            let diff = (y - scanner_y).abs();
            if diff <= radius {
                y_overlap = (radius - diff).abs();
                ranges.push(x - y_overlap..=x + y_overlap);
            }
        }

        ranges = merge_ranges(ranges);
        if ranges.len() > 1 {
            let beacon = (ranges[0].end() + 1, scanner_y as i32);
            let freq = beacon.0 as i64 * 4000000 + beacon.1 as i64;
            println!("Frequency: {freq}");
            break;
        }
        overlap_lines.push(ranges);
    }
}

fn merge_ranges(mut ranges: Vec<RangeInclusive<i32>>) -> Vec<RangeInclusive<i32>> {
    let mut merged: Vec<RangeInclusive<i32>> = Vec::new();
    ranges.sort_by(|a, b| a.start().cmp(b.start()));
    let mut ranges_iter = ranges.iter();
    let mut this_range = ranges_iter.next().unwrap().clone();
    loop {
        if let Some(next_range) = ranges_iter.next() {
            if *next_range.start() <= this_range.end()+1 {
                this_range = RangeInclusive::new(
                    min(*this_range.start(), *next_range.start()),
                    max(*this_range.end(), *next_range.end()),
                );
            } else if *next_range.start() > this_range.end() + 1 {
                merged.push(this_range.clone());
                this_range = next_range.clone();
            }
        } else {
            merged.push(this_range.clone());
            break;
        }
    }
    merged
}


#[derive(Debug)]
struct Sensor {
    location: (i32, i32),
    beacon: (i32, i32),
}

impl Sensor {
    fn radius(&self) -> i32 {
        taxicab_distance(self.location, self.beacon)
    }
}

fn taxicab_distance(a: (i32, i32), b: (i32, i32)) -> i32 {
    (a.1 - b.1).abs() + (a.0 - b.0).abs()
}

fn load_sensors() -> Vec<Sensor> {
    let file = File::open("input/day15.txt").unwrap();
    let lines = BufReader::new(file).lines();
    let mut sensors = Vec::new();

    let re = Regex::new(
        r"Sensor at x=([0-9-]+), y=([0-9-]+): closest beacon is at x=([0-9-]+), y=([0-9-]+)"
    ).unwrap();

    for line in lines {
        let line = line.unwrap();
        for cap in re.captures_iter(line.as_str()) {
            sensors.push(Sensor {
                location: (cap[1].parse::<i32>().unwrap(), cap[2].parse::<i32>().unwrap()),
                beacon: (cap[3].parse::<i32>().unwrap(), cap[4].parse::<i32>().unwrap()),
            });
        }
    }
    sensors
}