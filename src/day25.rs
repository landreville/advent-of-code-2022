use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

fn main() {
    let fuel = load_fuel();

    let total: isize = fuel.iter().map(|el| parse_snafu(el)).sum();
    println!("Total: {total}");
    let snafu_total = parse_int(total);
    println!("Snafu Total: {snafu_total}");
}

fn parse_int(acc: isize) -> String {
    if acc == 0 {
        return "".to_string();
    }

    match acc % 5 {
        remainder @ 0..=2 => [parse_int(acc / 5), remainder.to_string()].join(""),
        3 => [parse_int(1 + acc / 5), "=".to_string()].join(""),
        4 => [parse_int(1 + acc / 5), "-".to_string()].join(""),
        _ => panic!("Unexpected remainder"),
    }
}

fn parse_snafu(snafu: &str) -> isize {
    let mut acc = 0;
    for (i, ch) in snafu.chars().rev().enumerate() {
        let multiplier = if i == 0 { 1 } else { 5_isize.pow(i as u32) };
        let value = match ch {
            '2' => 2,
            '1' => 1,
            '0' => 0,
            '-' => -1,
            '=' => -2,
            _ => panic!("Invalid snafu"),
        };
        acc += multiplier * value;
    }
    acc
}

fn load_fuel() -> Vec<String> {
    let file = File::open("input/day25.txt").unwrap();
    let lines = BufReader::new(file).lines();
    let mut fuel = Vec::new();
    for line in lines {
        let line = line.unwrap();
        fuel.push(line.to_string());
    }

    fuel
}
