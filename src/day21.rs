use std::collections::HashMap;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

fn main() {
    let root = visit_monkey(&load_monkeys());
    let result = solve_monkey(&root, 0);
    println!("Result: {result}");
}

fn solve_monkey(monkey: &Monkey, value: isize) -> isize {
    match monkey {
        Monkey::Yell(_) => panic!("wtf"),
        Monkey::Human => value,
        Monkey::Math { op, a, b } => {
            let a_monk = *a.clone();
            let b_monk = *b.clone();
            match (a_monk, b_monk) {
                (a_monk, Monkey::Yell(x)) => solve_monkey(
                    &a_monk,
                    match op.as_str() {
                        "+" => value - x,
                        "*" => value / x,
                        "/" => value * x,
                        "-" => value + x,
                        "=" => x,
                        _ => panic!("Unknown op: {}", op),
                    },
                ),
                (Monkey::Yell(x), b_monk) => solve_monkey(
                    &b_monk,
                    match op.as_str() {
                        "+" => value - x,
                        "*" => value / x,
                        "/" => x / value,
                        "-" => x - value,
                        "=" => x,
                        _ => panic!("Unknown op: {}", op),
                    },
                ),
                _ => panic!("wtf"),
            }
        }
    }
}

fn visit_monkey(monkey: &Monkey) -> Monkey {
    match monkey {
        Monkey::Yell(val) => Monkey::Yell(*val),
        Monkey::Human => Monkey::Human,
        Monkey::Math { op, a, b } => {
            let a_monk = visit_monkey(a);
            let b_monk = visit_monkey(b);
            match (a_monk.clone(), b_monk.clone()) {
                (Monkey::Yell(a_val), Monkey::Yell(b_val)) => {
                    Monkey::Yell(apply_op(op, a_val, b_val))
                }
                _ => Monkey::Math {
                    op: op.to_string(),
                    a: Box::new(a_monk.clone()),
                    b: Box::new(b_monk.clone()),
                },
            }
        }
    }
}

fn apply_op(op: &str, a: isize, b: isize) -> isize {
    match op {
        "+" => a + b,
        "*" => a * b,
        "/" => a / b,
        "-" => a - b,
        _ => panic!("Unknown op: {}", op),
    }
}

#[derive(Debug, Clone)]
enum Monkey {
    Human,
    Yell(isize),
    Math {
        op: String,
        a: Box<Monkey>,
        b: Box<Monkey>,
    },
}

fn load_monkeys() -> Monkey {
    let file = File::open("input/day21.txt").unwrap();

    let lines = BufReader::new(file).lines();
    let mut proto_nodes = HashMap::new();

    for line in lines {
        let line = line.unwrap();
        let (name, rest) = line.split_once(": ").unwrap();
        proto_nodes.insert(name.to_string(), rest.to_string());
    }

    build_monkey("root", &proto_nodes)
}

fn build_monkey(name: &str, proto_nodes: &HashMap<String, String>) -> Monkey {
    let rest = proto_nodes.get(name).unwrap();

    if name == "humn" {
        Monkey::Human
    } else if let Ok(val) = rest.parse::<isize>() {
        Monkey::Yell(val)
    } else {
        Monkey::Math {
            a: Box::new(build_monkey(&rest[..4], &proto_nodes)),
            op: if name == "root" {
                "=".to_string()
            } else {
                rest[5..6].to_string()
            },
            b: Box::new(build_monkey(&rest[7..], &proto_nodes)),
        }
    }
}
