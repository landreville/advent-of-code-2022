use std::collections::VecDeque;
use std::fs;

const END: usize = 1000000000000;

fn main() {
    let gas = load_gas();
    let mut gas_iter = gas.iter().enumerate().cycle();
    let mut i: usize = 0;
    let mut height = 0;
    let rock_types = vec![
        vec![(0, 0), (1, 0), (2, 0), (3, 0)],
        vec![(0, 1), (1, 1), (2, 1), (1, 0), (1, 2)],
        vec![(0, 0), (1, 0), (2, 0), (2, 1), (2, 2)],
        vec![(0, 0), (0, 1), (0, 2), (0, 3)],
        vec![(0, 0), (1, 0), (0, 1), (1, 1)],
    ];
    let mut anchor: (i64, i64) = (2, 3);
    let width = 7;
    let mut grid = VecDeque::new();
    grid.push_back(vec![false; width]);
    let mut grid_start = 0;

    while i < END {
        let (dir_index, direction) = gas_iter.next().unwrap();

        let new_x = match direction {
            Direction::Left => anchor.0 - 1,
            Direction::Right => anchor.0 + 1
        };

        if new_x >= 0 && rock_types[i % 5].iter().all(|(current_x, current_y)| {
            let (x, y) = ((current_x + new_x) as usize, (current_y + anchor.1) as usize);
            x < width && (y > grid_start + grid.len() - 1 || !grid[y - grid_start][x])
        }) {
            anchor.0 = new_x;
        }
        let new_y = anchor.1 - 1;
        if new_y < 0 + grid_start as i64 || rock_types[i % 5].iter().any(|(current_x, current_y)| {
            let (x, y) = ((current_x + anchor.0) as usize, (current_y + new_y) as usize);
            y - grid_start < grid.len() && grid[y - grid_start][x]
        }) {
            for (x, y) in rock_types[i % 5].clone() {
                let (abs_x, abs_y) = ((anchor.0 + x) as usize, (anchor.1 + y) as usize);
                if abs_y + 1 > height {
                    height = abs_y + 1;
                }
                while abs_y - grid_start >= grid.len() {
                    grid.push_back(vec![false; width]);
                }
                grid[abs_y - grid_start][abs_x] = true;
            }


            let mut remove_until = 0;
            for (j, row) in grid.iter().rev().enumerate() {
                if remove_until == 0 && row.iter().all(|el| *el) {

                    grid_start = grid_start + grid.len() - j;
                    remove_until = grid.len() - j;
                    break;
                }
            }
            if remove_until > 0 {
                for _ in 0..remove_until {
                    grid.pop_front();
                }
            }

            anchor = (2, height as i64 + 3);
            i += 1;
        } else {
            anchor.1 = new_y
        }
    }

    println!("Height: {height}");
}

enum Direction {
    Left,
    Right,
}

fn load_gas() -> Vec<Direction> {
    fs::read_to_string("input/day17.txt").unwrap().chars()
        .filter_map(|ch| {
            match ch {
                '<' => Some(Direction::Left),
                '>' => Some(Direction::Right),
                _ => None
            }
        }).collect()
}