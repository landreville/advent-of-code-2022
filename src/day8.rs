use std::io::BufReader;
use std::fs::File;
use std::io::prelude::*;
use std::collections::HashSet;

fn main() {
    let trees = load_trees();
    part_one(&trees);
    part_two(&trees);
}

fn part_one(trees: &Vec<Vec<i32>>) {
    let tree_len = trees.len();
    let mut visible_trees: HashSet<(usize, usize)> = HashSet::new();

    let mut prev_tallest_by_row: i32;
    let mut prev_tallest_by_col: Vec<i32> = vec![-1; 100];

    // Left-Right Top-Bottom
    for (i, row) in trees.iter().enumerate() {
        prev_tallest_by_row = -1;
        for (j, tree) in row.iter().enumerate() {
            if j == 0 || *tree > prev_tallest_by_row {
                visible_trees.insert((i, j));
                prev_tallest_by_row = *tree;
            }
            if i == 0 || *tree > prev_tallest_by_col[j] {
                visible_trees.insert((i, j));
                prev_tallest_by_col[j] = *tree;
            }
        }
    }

    prev_tallest_by_col = vec![-1; 100];
    // Right to Left, Bottom to Top.
    for (i, row) in trees.into_iter().rev().enumerate() {
        prev_tallest_by_row = -1;
        for (j, tree) in row.iter().rev().enumerate() {
            if j == 0 || *tree > prev_tallest_by_row {
                visible_trees.insert((tree_len - i - 1, row.len() - j - 1));
                prev_tallest_by_row = *tree;
            }
            if i == 0 || *tree > prev_tallest_by_col[j] {
                visible_trees.insert((tree_len - i - 1, row.len() - j - 1));
                prev_tallest_by_col[j] = *tree;
            }
        }
    }

    let tree_count = visible_trees.len();
    println!("Visible Trees: {tree_count:?}");
}

fn part_two(trees: &Vec<Vec<i32>>) {
    let tree_len = trees.len();
    let mut the_best_tree_score = 0;
    let mut tree: &i32;
    let mut above;
    let mut left;
    let mut right;
    let mut below;
    let mut score;

    for i in 1..trees.len() - 2 {
        above = 1;
        left = 1;
        right = 1;
        below = 1;

        for j in 1..&trees[i].len() - 2 {
            tree = &trees[i][j];
            println!("Tree: {tree} ({i}, {j})");

            for above_i in (0..=i-1).rev() {
                if &trees[above_i][j] >= tree || above_i == 0{
                    above = i - above_i;
                    break;
                }
            }

            for below_i in i + 1..=tree_len-1 {
                if &trees[below_i][j] >= tree || below_i == tree_len-1{
                    below = below_i - i;
                    break;
                }
            }

            for left_j in (0..=j-1).rev() {
                if &trees[i][left_j] >= tree || left_j == 0{
                    left = j - left_j;
                    break;
                }
            }

            for right_j in j + 1..=&trees[i].len()-1 {
                if &trees[i][right_j] >= tree || right_j == &trees[i].len()-1 {
                    right = right_j - j;
                    break;
                }
            }

            println!("Top: {above} Right: {right} Bottom: {below} Left: {left}");
            score = above * left * below * right;
            if score > the_best_tree_score {
                the_best_tree_score = score;
            }
        }
    }

    println!("The best tree score: {the_best_tree_score}");
}

fn load_trees() -> Vec<Vec<i32>> {
    let file = File::open("input/day8.txt").unwrap();
    let lines = BufReader::new(file).lines();
    lines.map(
        |l| l.unwrap().chars().map(|c| c.to_digit(10).unwrap() as i32).collect()
    ).collect()
}