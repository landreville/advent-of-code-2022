use std::collections::{BinaryHeap, HashMap};
use std::fs::File;
use std::io::BufReader;
use std::io::BufRead;
use std::collections::HashSet;
use std::cmp::Ordering;

fn main() {
    let adj: HashMap<Node, HashSet<Node>> = load_graph();
    let mut distances = HashMap::new();
    let mut visited = HashSet::new();

    let mut start = Node { location: (0, 0), height: 'z' as i8 };
    for node in adj.keys() {
        if node.location == (120, 20) {
            start = *node;
        }
    }

    let mut to_visit = BinaryHeap::new();
    to_visit.push(Visit { node: start, distance: 0 });
    distances.insert(start.location, 0);

    while let Some(Visit { node, distance }) = to_visit.pop() {
        if !visited.insert(node) {
            continue;
        }

        if let Some(neighbours) = adj.get(&node) {
            for neighbour in neighbours {
                if neighbour.height < node.height - 1 {
                    continue;
                }
                let new_dist = distance + 1;
                let use_dist = distances.get(&neighbour.location)
                    .map_or(true, |&exist_dist| new_dist < exist_dist);
                if use_dist {
                    distances.insert(neighbour.location, new_dist);
                    to_visit.push(Visit { node: *neighbour, distance: new_dist });
                }
            }
        }
    }

    let mut lowest_dist = usize::MAX;
    for node in adj.keys() {
        if node.height != 'a' as i8 {
            continue;
        }
        if let Some(dist) = distances.get(&node.location){
            if dist < &lowest_dist {
                lowest_dist = *dist;
            }
        }
    }

    println!("Shortest a distance: {lowest_dist}");
}


#[derive(Eq, Hash, PartialEq, Clone, Copy, Debug)]
struct Node {
    location: (usize, usize),
    height: i8,
}

#[derive(Debug)]
struct Visit {
    node: Node,
    distance: usize,
}

impl Ord for Visit {
    fn cmp(&self, other: &Self) -> Ordering {
        other.distance.cmp(&self.distance)
    }
}

impl PartialOrd for Visit {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl PartialEq for Visit {
    fn eq(&self, other: &Self) -> bool {
        self.distance.eq(&other.distance)
    }
}

impl Eq for Visit {}

fn load_graph() -> HashMap<Node, HashSet<Node>> {
    let file = File::open("input/day12.txt").unwrap();
    let lines: Vec<String> = BufReader::new(file)
        .lines()
        .map(|l| l.unwrap())
        .collect();

    let mut adj: HashMap<Node, HashSet<Node>> = HashMap::new();
    let start_marker = 'S' as i8;
    let end_marker = 'E' as i8;

    for (y, line) in lines.iter().enumerate() {
        let row: Vec<i8> = line.chars()
            .map(|ch| ch as i8)
            .map(|height| {
                if height == start_marker {
                    'a' as i8
                } else if height == end_marker {
                    'z' as i8
                } else {
                    height
                }
            })
            .collect();

        for (x, height) in row.iter().enumerate() {
            let height = *height;

            let neighbours = adj.entry(
                Node { location: (x, y), height: height }
            ).or_insert(HashSet::new());

            // Left
            if x > 0 {
                neighbours.insert(
                    Node { location: (x - 1, y), height: row[x - 1] }
                );
            }
            // Right
            if x < row.len() - 1 {
                neighbours.insert(
                    Node { location: (x + 1, y), height: row[x + 1] }
                );
            }
            // Above
            if y > 0 {
                neighbours.insert(Node {
                    location: (x, y - 1),
                    height: lines[y - 1].chars().collect::<Vec<char>>()[x] as i8,
                });
            }
            // Below
            if y < lines.len() - 1 {
                neighbours.insert(Node {
                    location: (x, y + 1),
                    height: lines[y + 1].chars().collect::<Vec<char>>()[x] as i8,
                });
            }
        }
    }

    // for (node, neighbours) in adj.iter() {
    //     let loc = node.location;
    //     print!("{loc:?}:  ");
    //     for n in neighbours {
    //         let nloc = n.location;
    //         print!("{nloc:?} ");
    //     }
    //     print!("\n");
    // }
    adj
}